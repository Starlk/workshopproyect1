﻿using Microsoft.EntityFrameworkCore;
using WorkShopProyect.Models;

namespace WorkShopProyect.context
{
    public class AppDbContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options):base(options) { }
    }
}
