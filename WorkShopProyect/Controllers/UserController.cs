﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using WorkShopProyect.context;
using WorkShopProyect.Models;

namespace WorkShopProyect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly AppDbContext _context;

        public UserController(AppDbContext context) 
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<User>>> Get()
        {
            var sendUser = await _context.User.ToListAsync();
            return Ok(sendUser);
        }

        [HttpPost]
        public async Task<ActionResult<List<User>>> Post(User user)
        {
          _context.User.Add(user);
            await _context.SaveChangesAsync();
            return Ok(await _context.User.ToListAsync());
        }
    }
}
