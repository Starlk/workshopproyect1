﻿using FluentValidation;
using WorkShopProyect.Models;

namespace WorkShopProyect.Utils
{
    public class UserValidation : AbstractValidator<User>
    {
        public UserValidation()
        {
            RuleFor(user => user.Email).EmailAddress();
            RuleFor(user=>user.Password).NotEmpty();
        }
    }
}
