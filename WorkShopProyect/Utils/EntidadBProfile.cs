﻿using AutoMapper;
using WorkShopProyect.Models;

namespace WorkShopProyect.Utils
{
    public class EntidadBProfile : Profile
    {
        public EntidadBProfile()
        {
            CreateMap<EntidadA, EntidadB>()
                .ForMember(
                dest => dest.Id,
                opt => opt.MapFrom(src => src.Id)
                )
                .ForMember(
                dest => dest.Name,
                opt => opt.MapFrom(src => src.Name)
                )
                .ForMember(
                dest => dest.FechaCreated,
                opt => opt.MapFrom(src => src.FechaCreated)
                )
                .ForMember(
                dest => dest.IsBigCompany,
                opt => opt.MapFrom(src => src.IsBigCompany)
                )
                .ForMember(
                dest => dest.NumeroCompany,
                opt => opt.MapFrom(src => src.NumeroCompany)
                )
                .ForMember(
                dest => dest.Type,
                opt => opt.MapFrom(src => src.Type)
                )
                .ForMember(
                dest => dest.continet,
                opt => opt.MapFrom(src => src.country)
                )
                .ForMember(
                dest => dest.region,
                opt => opt.MapFrom(src => src.country2)
                )
                .ForMember(
                dest => dest.cantidad,
                opt => opt.MapFrom(src => src.count)
                )
                .ForMember(
                dest => dest.planet,
                opt => opt.MapFrom(src => src.country3)
                )
                .ForMember(
                dest => dest.Direccion,
                opt => opt.MapFrom(src => src.Dir)
                );
        }
    }
}
