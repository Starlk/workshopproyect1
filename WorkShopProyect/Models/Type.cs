﻿namespace WorkShopProyect.Models
{
    public class Type
    {
        public string Name { get; set; }
        public string Description { get; set; } 
    }
}
